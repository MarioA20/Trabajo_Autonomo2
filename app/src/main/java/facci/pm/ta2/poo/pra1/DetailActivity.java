package facci.pm.ta2.poo.pra1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;



import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView textViewNombre, textViewPrecio, textViewDescription;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        String object = getIntent().getStringExtra("object_id");
        textViewNombre = (TextView)findViewById(R.id.textViewNombre);
        textViewPrecio = (TextView) findViewById(R.id.textViewPrecio);
        textViewDescription = (TextView)findViewById(R.id.textViewDescription);
        imageView = (ImageView)findViewById(R.id.imagen);

        DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("objetoId");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    String precio = (String) object.get("price")+("\u0024");
                    String descripcion = (String) object.get("description");
                    String nombre = (String) object.get("name");
                    Bitmap imagenBitmap = (Bitmap) object.get("image");
                    textViewPrecio.setText(precio);
                    textViewDescription.setText(descripcion);
                    textViewNombre.setText(nombre);
                    imageView.setImageBitmap(imagenBitmap);

                }else {
                    //error
                }
            }
        });
        // FIN - CODE6
    }
}
